using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class OutlineSelectionResponse : MonoBehaviour, ISelectionResponse
{
    // Outlines object using Quick Outline Asset
    public void OnSelect(Transform selection)
    {
        var outline = selection.GetComponent<Outline>();
        // Initializing interactionPopup
        GameObject interactPopup = null;
        // If object is selectable it must have the interactPopup as
        // child
        if (selection.CompareTag("Selectable"))
        {
            // iterate through children to find the interaction popup
            for(int i = 0; i < selection.transform.childCount; i++)
            {
                if (selection.transform.GetChild(i).gameObject.name == "InteractPopup")
                    interactPopup = selection.transform.GetChild(i).gameObject;
            }
        }
        if (interactPopup != null)
        {
            interactPopup.GetComponent<MeshRenderer>().enabled = true;
        }
        // Check if there's a captured object, Outline width = 20
        if (outline != null)
            outline.OutlineWidth = 20;
    }

    public void OnInteract(Transform selection)
    {
        // Switch out the two outlines (interact outline now takes over)
        var outline = selection.GetComponent<Outline>();
        var interOutline = selection.GetComponent<InteractOutline>();
        if (interOutline != null) interOutline.OutlineWidth = 20;
        if (outline      != null) outline.OutlineWidth = 0;
    }

    public void OnStopInteract(Transform selection)
    {
        // Switch out the two outlines (normal outline now takes over)
        var outline = selection.GetComponent<Outline>();
        var interOutline = selection.GetComponent<InteractOutline>();
        if (interOutline != null) interOutline.OutlineWidth = 0;
        if (outline      != null) outline.OutlineWidth = 20;
    }

    // Removes outlined object using Quick Outline Asset
    public void OnDeselect(Transform selection)
    {
        GameObject interactPopup = null;
        if (selection.CompareTag("Selectable"))
        {
            // iterate through children to find the interaction popup
            for(int i = 0; i < selection.transform.childCount; i++)
            {
                if (selection.transform.GetChild(i).gameObject.name == "InteractPopup")
                    interactPopup = selection.transform.GetChild(i).gameObject;
            }
        }
        if (interactPopup != null)
            interactPopup.GetComponent<MeshRenderer>().enabled = false;
        // Check if there's a captured object, Both Outlines = 0
        var interOutline = selection.GetComponent<InteractOutline>();
        var outline = selection.GetComponent<Outline>();
        if (interOutline != null) interOutline.OutlineWidth = 0;
        if (outline != null) outline.OutlineWidth = 0;
    }
}

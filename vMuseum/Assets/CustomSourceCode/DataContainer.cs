using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataContainer: MonoBehaviour
{
    [SerializeField] private string imName;
    [SerializeField] private string imCollection;
    [SerializeField] private float imPrice;
    [SerializeField] private string imInfo;

    public string GetImName(){
        return imName;
    }

    public string GetImCollection(){
        return imCollection;
    }

    public float GetImPrice(){
        return imPrice;
    }

    public string GetImInfo(){
        return imInfo;
    }

    public void SetImName(string imName)
    {
        this.imName = imName;
    }

    public void SetImCollection(string imCollection)
    {
        this.imCollection = imCollection;
    }

    public void SetImPrice(float imPrice)
    {
        this.imPrice = imPrice;
    }

    public void SetImInfo(string imInfo)
    {
        this.imInfo = imInfo;
    }

    public DataContainer() { }

    public DataContainer(string imName, string imCollection, float imPrice, string imInfo)
    {
        this.imName = imName;
        this.imCollection = imCollection;
        this.imPrice = imPrice;
        this.imInfo = imInfo;
    }
}

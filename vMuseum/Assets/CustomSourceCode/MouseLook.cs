using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    [SerializeField] private float mouseSensitivity = 100f;
    
    private Transform playerBody;
     
    float xRotation = 0f;

    // Start is called before the first frame update
    void Start()
    {
        //Cursor.lockState = CursorLockMode.Locked;   // Disables the cursor, press esc to enable it
        playerBody = transform.parent;      // Gets parental GameObject transform
    }

    // Update is called once per frame
    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);      // Limits mouse rotation to -90 to 90 degrees

        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);      // Mouse vertical rotation
        playerBody.Rotate(Vector3.up * mouseX);     // Mouse horizotal rotation
    }
}

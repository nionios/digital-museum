using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private static bool PauseMenuVisible = false;
    [SerializeField] private GameObject pauseMenuUi;
    private MonoBehaviour[] movementScripts = new MonoBehaviour[2];
    private GameObject InspectorWindow;   

    public void Awake(){
        GameObject FirstPersonPlayerObj = GameObject.Find("FirstPersonPlayer");
        movementScripts[0] = FirstPersonPlayerObj.GetComponent<PlayerMovement>();
        movementScripts[1] = FirstPersonPlayerObj.transform.Find("Main Camera").GetComponent<MouseLook>();
        InspectorWindow = GameObject.Find("InteractWindow");
    }

    private void toggle(bool state){        // (De)Activates movement scripts 
        foreach (MonoBehaviour obj in movementScripts)
            obj.enabled = state;
    }

    public void Update(){
        if (Input.GetKeyDown(KeyCode.Escape)){        // Key Q was pressed        !!! ESCAPE KEYCODE ON FINAL PRODUCT !!! 
            if (PauseMenuVisible)
                ContinueButton();    
            else
                PausedApplication();
        }
    }

    public void ContinueButton(){
        PauseMenuVisible = false;
        pauseMenuUi.SetActive(false);       // Disbling Main Menu (blurryLiveBackground GameObject)
        if(InspectorWindow.GetComponent<InteractWindow>().GetInteractWindowVisibleVal()){
            Cursor.lockState = CursorLockMode.None;       // Locking cursor when disabling the main menu
            toggle(false);
            return;
        }  
        toggle(true);
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void PausedApplication(){
        Cursor.lockState = CursorLockMode.None;         // Unlocking cursor when disabling the main menu
        Cursor.visible = true;
        pauseMenuUi.SetActive(true);        // Enabling Main Menu (blurryLiveBackground GameObject)
        toggle(false);
        PauseMenuVisible = true;
    }
    
    public void QuitButton() {
        Application.Quit();
        Debug.Log("Exited application");
    }
}
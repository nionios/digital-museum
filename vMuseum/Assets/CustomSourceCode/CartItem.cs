using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CartItem
{
    [SerializeField] private string itemName;
    [SerializeField] private float itemPrice;

    public string GetItemName(){
        return itemName;
    }

    public float GetItemPrice(){
        return itemPrice;
    }

    public void SetItemName(string itemName)
    {
        this.itemName = itemName;
    }

    public void SetItemPrice(float itemPrice)
    {
        this.itemPrice = itemPrice;
    }

    public CartItem() { }

    public CartItem(string itemName,  float itemPrice)
    {
        this.itemName = itemName;
        this.itemPrice = itemPrice;
    }
}

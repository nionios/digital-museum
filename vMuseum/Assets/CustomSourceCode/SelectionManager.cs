﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionManager : MonoBehaviour
{
    // Tag of interest
    [SerializeField] private string selectableTag = "Selectable";
    [SerializeField] private GameObject PauseMenu;
    [SerializeField] private GameObject HelpMenu;
    // This sets the distance from which the player is allowed to interact with
    // an object
    [SerializeField][Range(1, 50)] private float rayDistance = 20.0f;

    private ISelectionResponse selectionResponse;
    private Transform selection;
    private InteractWindowInterface InteractWindowScript;

    private void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;
        // Cursor.visible = false;
        selectionResponse = GetComponent<ISelectionResponse>();
        /* Get the script that opens the InteractionWindow, store the reference
         * for later */
        InteractWindowScript = GameObject.Find("InteractWindow").GetComponent<InteractWindow>();
    }

    private Transform GetObjectThroughRay ()
    {
        // Setting a variable for ray
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        // RaycastHit is a player click
        RaycastHit hit = new RaycastHit();
        // Gets objects (distance rayDistance) with cursor
        if (Physics.Raycast(ray, out hit, rayDistance))
        {
            var ScannedSelection = hit.transform;
            // Checks if selected object from Raycast has the same tag as given
            // above.
            if (ScannedSelection.CompareTag(selectableTag)) {
                selection = ScannedSelection;
                return selection;
            }
        }
        // If no selectable object is found, return null
        return null;
    }

    private void Update()
    {
        // Check's if Pause menu is enabled or not, if yes, return
        if (PauseMenu.activeSelf || HelpMenu.activeSelf) return;
        /* Deselect window and null out selection. This will be undone
         * immediately buy only if we are selecting something. */
        if (selection != null)
            selectionResponse.OnDeselect(selection);
        // Scan for object through Raycast
        selection = GetObjectThroughRay();
        // For checking if the interaction window is open
        bool InteractWindowVisible = InteractWindowScript.GetInteractWindowVisibleVal();
        /* We do not care what the selection is, if E is pressed while the
         * InteractionWindow is visible, close the window. */
        if (InteractWindowVisible)
        {
            if (Input.GetKeyDown(KeyCode.E))
                InteractWindowScript.CloseWindow();
            if (Input.GetKeyDown(KeyCode.A))
                InteractWindowScript.AddToCart();
        } else {
            // If there's a specified image captured then outline is applied
            if (selection != null)
            {
                selectionResponse.OnSelect(selection);
                /* If user clicks or presses "E", start sequence for trying to
                 * buy the art */
                if (Input.GetKeyDown(KeyCode.E))
                {
                    selectionResponse.OnInteract(selection);
                    // Open the interaction window now
                    InteractWindowScript.OpenWindow();
                    InteractWindowScript.PopulateWindow(selection);
                }
            }
        }
    }
}

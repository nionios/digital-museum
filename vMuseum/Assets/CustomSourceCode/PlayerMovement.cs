using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    private CharacterController controller;

    [SerializeField] float speed = 10f;

    Vector3 velocity;

    // Start is called before the first frame update
    void Start()
    {
       controller = gameObject.GetComponent<CharacterController>();     // Gets GameObject's CharacterController used for local space movement
    }

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;     // Calculating new vector after any horizontal/vertical change (awsd or arrow keys)

        if(Input.GetKey(KeyCode.LeftShift))
            controller.Move(move * speed * Time.deltaTime * 1.5f);     // Perform movements
        else
            controller.Move(move * speed * Time.deltaTime);     // Perform movements

        velocity.y += (-9.81f) * Time.deltaTime * Time.deltaTime;       // Calculating gravity force 

        controller.Move(velocity);     // Gravity applied
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowUI : MonoBehaviour
{
    public string str;
    private Text textElement;
    private RawImage janitorImage;
    private RawImage soundImage;
    private new AudioSource audio;
    private bool isInside = false;
    private bool wasPlaying = false;
    [SerializeField] 
    private GameObject[] blurryLiveBackground;
    [SerializeField] 
    private GameObject interact;

    // Start is called before the first frame update
    [System.Obsolete]
    void Start(){       // Initializing janitor states 
        textElement = GameObject.FindWithTag("textElement").GetComponent<Text>();
        janitorImage = GameObject.FindWithTag("janitorImage").GetComponent<RawImage>();
        soundImage = GameObject.FindWithTag("soundImage").GetComponent<RawImage>();
        textElement.enabled = false;
        janitorImage.enabled = true;
        janitorImage.color = new Color(janitorImage.color.r, janitorImage.color.g, janitorImage.color.b, 0.0f);
        soundImage.enabled = false;
        audio = GetComponent<AudioSource>();
        audio.Stop();
    }

    private void Update(){

        if (!isInside)
            return;

        if ( returnIfWindowsAreEnabled())
        {      // If player is not inside then update becomes useless
            if (audio.isPlaying)
            {
                //Debug.Log("audio was playing");
                wasPlaying = true;
                audio.Pause();
            }
            textElement.enabled = false;
            soundImage.enabled = false;
            janitorImage.enabled = false;
            return;
        }

        if (wasPlaying)
        {
            wasPlaying = false;
            audio.UnPause();
            textElement.enabled = true;
            soundImage.enabled = true;
            janitorImage.enabled = true;
            return;
        }
        
        // inside and not windows enabled
        AudioPlay();
        /*
        if (!isInside || returnIfWindowsAreEnabled()){      // If player is not inside then update becomes useless
            if (audio.isPlaying){ 
                //Debug.Log("audio was playing");
                wasPlaying = true;
                audio.Pause();
            }
            textElement.enabled = false;
            soundImage.enabled = false;
            janitorImage.enabled = false;
            return;
        }
        
        AudioPlay();

        if (wasPlaying){       // If audio was interrupted
            wasPlaying = false;
            soundImage.enabled = true;
            audio.UnPause();
        }
        
        if (isInside && !returnIfWindowsAreEnabled()){      // If other windows are disabled and player is inside a trigger
            textElement.enabled = true;
            janitorImage.enabled = true;
        }*/
    }

    private void triggerFlipper(bool state, float colorNu){         // Set states when player enters / leaves trigger
        isInside = state;
        janitorImage.color = new Color(janitorImage.color.r, janitorImage.color.g, janitorImage.color.b, colorNu);
        textElement.enabled = state;
        janitorImage.enabled = state;
    }

    void OnTriggerEnter(Collider player){  
        if (player.gameObject.tag == "Player")
        {
            triggerFlipper(true, 0.3f);
            Debug.Log("Enter with -> " + str);
        }
            
            
    }

    private void OnTriggerExit(Collider player){        // "Disables" all features
        if (player.gameObject.tag == "Player"){
            triggerFlipper(false, 0.0f);
            soundImage.enabled = false;
            audio.Stop();
            Debug.Log("Exit with -> " + str);
        }
    }
    
    private void AudioPlay(){
        if (Input.GetKeyDown(KeyCode.Space)){       // When space is pressed a speaker image is previewed, penguin changes color and audio track plays
            soundImage.enabled = true;      // Speaker image is previewed
            janitorImage.color = new Color(janitorImage.color.r, janitorImage.color.g, janitorImage.color.b, 1.0f);     // Penguin changes color
            audio.Play();       // Audio track plays
        }      
        if (isInside && !audio.isPlaying && !wasPlaying){       // For disabling the speaker image when track has stopped
            soundImage.enabled = false;
            janitorImage.color = new Color(janitorImage.color.r, janitorImage.color.g, janitorImage.color.b, 0.3f);
        }
    }

    private bool returnIfWindowsAreEnabled(){       // Checks if Pause menu OR Live help is enabled
        if(interact.GetComponent<InteractWindow>().GetInteractWindowVisibleVal())
            return true;
        foreach (GameObject AudioPlay in blurryLiveBackground)
            if (AudioPlay.activeSelf)
                return true;
        return false;
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

internal interface InteractWindowInterface
{
    void freezePlayer();
    void unfreezePlayer();
    void Start();
    void OpenWindow();
    void CloseWindow();
    void PopulateWindow(Transform selection);
    void AddToCart();
    bool GetInteractWindowVisibleVal();
}

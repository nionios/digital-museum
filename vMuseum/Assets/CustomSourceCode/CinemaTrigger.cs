using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class CinemaTrigger : MonoBehaviour
{
    public GameObject cinema;
    public GameObject layout;
    public GameObject [] priorityObj;
    private bool isInside = false;
    private VideoPlayer video;
    public Text text;
    [SerializeField]
    private GameObject interact;

    // Start is called before the first frame update
    void Start()
    {
        video = cinema.GetComponent<VideoPlayer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!CheckPriority())
        {
            if (isInside)
            {
                layout.SetActive(true);
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    if (video.isPlaying)
                    {
                        video.Pause();
                        text.text = "Press space to PLAY";
                    }
                    else
                    {
                        video.Play();
                        text.text = "Press space to PAUSE";
                    }
                }
            }
        }
        else
        {
            layout.SetActive(false);
        }
        
    }

    private bool CheckPriority()
    {
        if(interact.GetComponent<InteractWindow>().GetInteractWindowVisibleVal())
            return true;
        foreach(GameObject tmp in priorityObj)
        {
            if (tmp.activeSelf)
            {
                return true;
            }
        }
        return false;
    }

    private void OnTriggerEnter(Collider player)
    {
        if (player.tag == "Player")
        {
            isInside = true;
            layout.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider player)
    {
        if (player.tag == "Player")
        {
            isInside = false;
            layout.SetActive(false);
        }
    }
}

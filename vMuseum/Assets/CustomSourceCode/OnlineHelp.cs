using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnlineHelp : MonoBehaviour
{
    [SerializeField] private static bool OnlineHelpVisible = false;
    [SerializeField] private GameObject OnlineHelpUi;
    [SerializeField] private GameObject pauseMenu; // BlurryLiveBackground of pauseMenu
    private GameObject InspectorWindow;   
    private MonoBehaviour[] movementScripts = new MonoBehaviour[2];         // Movement Scripts 

    public void Awake(){
        InspectorWindow = GameObject.Find("InteractWindow"); 
        GameObject FirstPersonPlayerObj = GameObject.Find("FirstPersonPlayer");
        movementScripts[0] = FirstPersonPlayerObj.GetComponent<PlayerMovement>();
        movementScripts[1] = FirstPersonPlayerObj.transform.Find("Main Camera").GetComponent<MouseLook>();
    }

    public void Update(){
        if (Input.GetKeyDown(KeyCode.Escape) || pauseMenu.activeSelf){       // !!! ESCAPE KEYCODE ON FINAL PRODUCT !!!
            if(OnlineHelpUi.activeSelf){
                OnlineHelpUi.SetActive(false);
                OnlineHelpVisible = false;
            }
            return;
        }
        
        if (Input.GetKeyDown(KeyCode.H))        // Key H was pressed
            if (OnlineHelpVisible)  
                GoBackButton();
            else
                PausedApplication();
    }

    private void toggle(bool state){        // (De)Activates movement scripts 
        foreach (MonoBehaviour obj in movementScripts)
            obj.enabled = state;
    }

    public void GoBackButton(){
        //Cursor.visible = false;
        OnlineHelpUi.SetActive(false);       // Disbling Main Menu (blurryLiveBackground GameObject)
        OnlineHelpVisible = false;
        if(InspectorWindow.GetComponent<InteractWindow>().GetInteractWindowVisibleVal()){
            Cursor.lockState = CursorLockMode.None;       // Locking cursor when disabling the main menu   
            toggle(false);
            return;
        }
        toggle(true);
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void PausedApplication(){
        toggle(false);
        Cursor.lockState = CursorLockMode.None;         // Unlocking cursor when disabling the main menu
        Cursor.visible = true;
        OnlineHelpUi.SetActive(true);        // Enabling Main Menu (blurryLiveBackground GameObject)
        OnlineHelpVisible = true;
    }
}
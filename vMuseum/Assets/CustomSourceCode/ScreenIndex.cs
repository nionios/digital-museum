using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScreenIndex : MonoBehaviour
{
    private GameObject[] buttons = new GameObject[4];

    [SerializeField] 
    private GameObject[] popUps;        // Order: Pause Menu, Online Help, Interact Window
    private bool state = true;      // If theres at least one disabled GameObject (false = Disabled GameObject)

    public void Awake(){
        buttons[0] = GameObject.Find("ScreenIndex/helpPair");
        buttons[1] = GameObject.Find("ScreenIndex/pausePair");
        buttons[2] = GameObject.Find("ScreenIndex/cartPair");
        buttons[3] = GameObject.Find("ScreenIndex/center");
    }

    public void Update(){
        if(popUps[0].activeSelf){       // If Pause Menu was enabled, diasble all components
            state = false;
            foreach(GameObject obj in buttons)
                obj.SetActive(false);
            return;
        }else if(popUps[1].activeSelf || popUps[2].GetComponent<InteractWindow>().GetInteractWindowVisibleVal() || popUps[3].activeSelf){
            state = false;      // If Interaction Window or Online help is enabled, disable only the crosshair
            for(int i = 0; i < buttons.Length - 1; i++)
                buttons[i].SetActive(true);
            buttons[3].SetActive(false);
            return;
        }
        if(!state){      // If all PopUps are disabled and State == false then GameObjects are enabled
            state = true;
            foreach(GameObject obj in buttons)
                if(!obj.activeSelf)
                    obj.SetActive(true);
        }
    }
}   

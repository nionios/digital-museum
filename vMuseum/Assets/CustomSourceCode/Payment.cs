using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Payment : MonoBehaviour
{
    public int cnt = 0;

    private GameObject InspectorWindow;  
    private MonoBehaviour[] movementScripts = new MonoBehaviour[2];         // Movement Scripts 
    [SerializeField] private GameObject [] customerDataForm = new GameObject[8];
    [SerializeField] private GameObject[] cardDataForm = new GameObject[4];
    [SerializeField] private List<CartItem> cartItems;
    // List of cart items based on CartItem Obj
    public GameObject[] priorityGameObj;
    [SerializeField] private GameObject[] openWind = new GameObject[2];
    public GameObject cartInfo;    // panel
    public GameObject customerData;// panel
    public GameObject cardData;    // panel
    public GameObject totalForm;   // panel
    public GameObject orderInfo;   // panel
    public GameObject proccedButton;
    public GameObject emptyCartText;
    public GameObject scrollArea;
    public Text scrollAreaItems;
    //public GameObject pauseMenu;

    public Button proccedToPayment;
    public Button nextStep;
    public Button pay;
    public Button close;
    public Button backClose;
    public Button backToCart;
    public Button backToCustomerData;
    public TextMeshProUGUI infoText;

    // This is set when the cursor was invisible before the window was open
    private bool returnCursorToInvisibilityFlag = false;
    //private bool state = false;
    private bool isActive = false;
    private string customersInfo;
    private string cartInfoStr;

    void Awake()
    {
        //cartItems = new List<CartItem>();
        //Debug.Log(cartItems.Any());
        InspectorWindow = GameObject.Find("InteractWindow"); 
        GameObject FirstPersonPlayerObj = GameObject.Find("FirstPersonPlayer");
        movementScripts[0] = FirstPersonPlayerObj.GetComponent<PlayerMovement>();
        movementScripts[1] = FirstPersonPlayerObj.transform.Find("Main Camera").GetComponent<MouseLook>();
    }
    // Start is called before the first frame update
    void Start()
    {
        ReceiveCartItems();
        proccedToPayment.onClick.AddListener(ProccedToPayment);
        nextStep.onClick.AddListener(CustomerForm);
        pay.onClick.AddListener(PaymentForm);
        close.onClick.AddListener(Close);
        backClose.onClick.AddListener(Close);
        backToCart.onClick.AddListener(BackToCart);
        backToCustomerData.onClick.AddListener(BackToCustomerData);
    }

    void ProccedToPayment()
    {
        cartInfo.SetActive(false);
        customerData.SetActive(true);
    }

    void CustomerForm()
    {
        int counter = CheckInputFields(customerDataForm);

        if (counter == 0) // all customerDataForm complete so go on
        {
            customersInfo = customerDataForm[0].GetComponent<InputField>().text + " " + customerDataForm[1].GetComponent<InputField>().text +
                "\n" + customerDataForm[2].GetComponent<InputField>().text +
                "\n" + customerDataForm[3].GetComponent<InputField>().text +
                "\n" + customerDataForm[4].GetComponent<InputField>().text + " " + customerDataForm[5].GetComponent<InputField>().text +
                "\n" + customerDataForm[6].GetComponent<InputField>().text + " " + customerDataForm[7].GetComponent<InputField>().text;
            customerData.SetActive(false);
            cardData.SetActive(true);
        }
        //Debug.Log("OnClick");
    }

    void PaymentForm()
    {
        int counter = CheckInputFields(cardDataForm);
        if (counter == 0) // all customerDataForm complete so go on
        {
            cardData.SetActive(false);
            infoText.text = "Successful order!!!\n\n" + customersInfo;
            orderInfo.SetActive(true);
            //cartItems = new List<CartItem>();
        }
    }

    private void flip(bool state){
        foreach (GameObject obj in openWind)
            obj.SetActive(state);
    }

    private int CheckInputFields(GameObject [] array)
    {
        InputField inF;
        int counter = 0;
        foreach (GameObject tmp in array)
        {
            inF = tmp.GetComponent<InputField>();
            if (inF.text == "")
            {
                inF.placeholder.color = new Color(255, 0, 0, 128);
                counter++;
            }
        }
        return counter;
    }

    private void toggle(bool state){        // (De)Activates movement scripts 
        foreach (MonoBehaviour obj in movementScripts)
            obj.enabled = state;
    }
    
    void Close()
    {
        flip(true);
        totalForm.SetActive(false);
        // state = false;
        isActive = false;
        // If this is true then make cursor invisible after execution
        if (returnCursorToInvisibilityFlag)
        {
            Cursor.visible = false;
            returnCursorToInvisibilityFlag = false;
        }
        if(InspectorWindow.GetComponent<InteractWindow>().GetInteractWindowVisibleVal()){
            Cursor.lockState = CursorLockMode.None;       // Locking cursor when disabling the main menu   
            toggle(false);
            return;
        }
        toggle(true);
        Cursor.lockState = CursorLockMode.Locked;
    }

    void BackToCart()
    {
        customerData.SetActive(false);
        cartInfo.SetActive(true);
    }

    void BackToCustomerData()
    {
        cardData.SetActive(false);
        customerData.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (!CheckIfOthersOpen())
        {
            // if anything else opens do not open
            if (Input.GetKeyDown(KeyCode.C) && !isActive)
            {
                // If this is true then make cursor invisible after execution
                if (!Cursor.visible)
                {
                    Cursor.visible = true;
                    returnCursorToInvisibilityFlag = true;
                }
                Cursor.lockState = CursorLockMode.None;
                flip(false);
                toggle(false);
                emptyCartText.SetActive(false);
                scrollArea.SetActive(false);
                totalForm.SetActive(true);
                cartInfo.SetActive(true);

                Debug.Log("Cart count: " + cartItems.Count);

                if (cartItems.Count == 0)
                {
                    proccedButton.SetActive(false);
                    emptyCartText.SetActive(true);
                } else {
                    cartInfoStr = "";

                    foreach (CartItem tmp in cartItems)
                    {
                        cartInfoStr += tmp.GetItemName()  +
                                       " (Price: "        +
                                       tmp.GetItemPrice() +
                                       ")\n";
                    }
                    scrollAreaItems.text = cartInfoStr;
                    scrollArea.SetActive(true);
                    proccedButton.SetActive(true);
                }
                customerData.SetActive(false);
                cardData.SetActive(false);
                orderInfo.SetActive(false);
                isActive = true;
            // If cart is open then close it with C keypress
            } else if (Input.GetKeyDown(KeyCode.C) && isActive) Close();

            if (Input.GetKeyDown(KeyCode.Tab) && customerData.activeSelf)
            {
                NextTab(customerDataForm);
            }
            else if (Input.GetKeyDown(KeyCode.Tab) && cardData.activeSelf)
            {
                NextTab(cardDataForm);
            }

            if (customerData.activeSelf)
            {
                customerDataForm[0] = GameObject.Find("First name");
                customerDataForm[1] = GameObject.Find("Last name");
                customerDataForm[2] = GameObject.Find("Phone number");
                customerDataForm[3] = GameObject.Find("e-mail");
                customerDataForm[4] = GameObject.Find("Street address");
                customerDataForm[5] = GameObject.Find("ZIP code");
                customerDataForm[6] = GameObject.Find("City");
                customerDataForm[7] = GameObject.Find("Region");
            }

            if (cardData.activeSelf)
            {
                cardDataForm[0] = GameObject.Find("Name");
                cardDataForm[1] = GameObject.Find("date");
                cardDataForm[2] = GameObject.Find("CardNumber");
                cardDataForm[3] = GameObject.Find("CVV");
            }
        } else {
            totalForm.SetActive(false);
            isActive = false;
        }
    }

    private bool CheckIfOthersOpen()
    {
        foreach(GameObject tmp in priorityGameObj)
        {
            if (tmp.activeSelf)
            {
                return true;
            }
        }
        return false;
    }

    void NextTab(GameObject [] arr)
    {
        int i;
        for(i = 0; i < arr.Length; i++)
        {
            if (arr[i].GetComponent<InputField>().isFocused)
            {
                if(i >= arr.Length - 1)
                {
                    arr[0].GetComponent<InputField>().Select();
                }
                else
                {
                    arr[i + 1].GetComponent<InputField>().Select();
                }
                return;
            }
        }
        arr[0].GetComponent<InputField>().Select();
    }

    public void ReceiveCartItems()
    {
        List<CartItem> ReceivedCartItems = GameObject.Find("InteractWindow").GetComponent<InteractWindow>().GetCartItems();
        if (ReceivedCartItems != null) 
        {
            Debug.Log("Items Added to Cart!");
            cartItems = ReceivedCartItems;
            cnt = cnt + 1;
            Debug.Log(cartItems.Count + " size");
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InteractWindow : MonoBehaviour, InteractWindowInterface
{
    [SerializeField] private bool InteractWindowVisible;
    [SerializeField] private Canvas InteractWindowCanvasComponent;
    [SerializeField] private GameObject InteractWindowObj;

    public List<CartItem> cartItems = new List<CartItem>();
    private MonoBehaviour playerMovement;
    private MonoBehaviour mouseLook;

    public void freezePlayer() {
        // Player is paralyzed
        if (playerMovement != null) playerMovement.enabled = false;
        if (mouseLook != null) mouseLook.enabled = false;
    }

    public void unfreezePlayer() {
        // Player regains movement
        if (playerMovement != null) playerMovement.enabled = true;
        if (mouseLook != null) mouseLook.enabled = true;
    }

    public void Start()
    {
        // Find InteractWindow GameObject and store the reference to its canvas
        InteractWindowObj = GameObject.Find("InteractWindowCanvas");
        InteractWindowCanvasComponent = InteractWindowObj.GetComponent<Canvas>();
        InteractWindowVisible = false;
        InteractWindowCanvasComponent.enabled = false;
        GameObject FirstPersonPlayerObj = GameObject.Find("FirstPersonPlayer");
        // Get the playerMovement script component ready for disabling/enabling
        playerMovement = FirstPersonPlayerObj.GetComponent<PlayerMovement>();
        // Get the MouseLook script component ready for disabling/enabling
        mouseLook = FirstPersonPlayerObj.transform.Find("Main Camera").GetComponent<MouseLook>();
    }

    public void OpenWindow()
    {
        // Window starts to exist right here
        // Unlocking cursor when disabling the window
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        InteractWindowVisible = true;
        InteractWindowCanvasComponent.enabled = true;
        freezePlayer();
    }

    public void CloseWindow()
    {
        // Locking cursor when disabling the window
        Cursor.lockState = CursorLockMode.Locked;
        InteractWindowVisible = false;
        InteractWindowCanvasComponent.enabled = false;
        unfreezePlayer();
    }

    public void PopulateWindow(Transform selection)
    {
        // Get stored data about the painting
        string RetrievedName       = selection.GetComponent<DataContainer>().GetImName();
        string RetrievedCollection = selection.GetComponent<DataContainer>().GetImCollection();
        string RetrievedInfo       = selection.GetComponent<DataContainer>().GetImInfo();
        float  RetrievedPrice      = selection.GetComponent<DataContainer>().GetImPrice();
        // Populate the window with retrieved info
        if (RetrievedCollection != "")
            InteractWindowObj.transform.Find("Panel/Mutables/PaintingCollection").GetComponent<Text>().text = RetrievedCollection;
        if (RetrievedInfo != "")
            InteractWindowObj.transform.Find("Panel/Mutables/ScrollView/Viewport/PaintingInfo").GetComponent<Text>().text = RetrievedInfo;
        if (RetrievedPrice != 0.0)
            InteractWindowObj.transform.Find("Panel/Mutables/PaintingPrice").GetComponent<Text>().text = RetrievedPrice.ToString();
        if (RetrievedName != "")
        {
            InteractWindowObj.transform.Find("Panel/Mutables/PaintingTitle").GetComponent<Text>().text  = RetrievedName;
            string TargetSprite = "Images/" + RetrievedName;
            Debug.Log("My name is " + TargetSprite);
            /* Load Sprite with the same name as the material of the painting
             * object from disk and display it as the thumbnail.*/
            Sprite LoadedSprite = Resources.Load<Sprite>(TargetSprite);
            InteractWindowObj.transform.Find("Panel/Mutables/PaintingThumbnail").GetComponent<Image>().sprite = LoadedSprite;
        }
    }

    public void AddToCart () {
        string inputName =
            InteractWindowObj.transform.Find("Panel/Mutables/PaintingTitle").GetComponent<Text>().text;
        float inputPrice =
            (float) Convert.ToDouble(InteractWindowObj.transform.Find("Panel/Mutables/PaintingPrice").GetComponent<Text>().text);
        CartItem inputCartItem = new CartItem(inputName, inputPrice);
        cartItems.Add(inputCartItem);
    }

    public List<CartItem> GetCartItems() {
        return cartItems;
    }

    public bool GetInteractWindowVisibleVal() {
        return InteractWindowVisible;
    }

}
